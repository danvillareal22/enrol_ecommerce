<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

defined('MOODLE_INTERNAL') || die();

function enrol_ecommerce_enrol_user($product, $checkout) {
    global $DB, $USER;

    if (!enrol_is_enabled('ecommerce')) {
        return;
    }

    $plugin = enrol_get_plugin('ecommerce');
    $startdate = time();
    $timeend = 0;
    $userid = $checkout->userid;

    if (!$defaultrole = $plugin->get_config('roleid', 0)) {
        $student = get_archetype_roles('student');
        $student = reset($student);
        $defaultrole = $student->id;
    }

    $plugin = enrol_get_plugin('ecommerce');

    $courses = \local_ecommerce\product::get_product_courses($product);

    if ($courses) {
        foreach ($courses as $course) {

            // prepare enrollment duration
            if (!$startdate = get_config('enrol_ecommerce', 'enrolstart')) {
                // Default to now if there is no system setting.
                $startdate = 4;
            }
            switch($startdate) {
                case 2:
                    $timestart = $course->startdate;
                    break;
                case 4:
                    // We mimic get_enrolled_sql round(time(), -2) but always floor as we want users to always access their
                    // courses once they are enrolled.
                    $timestart = intval(substr(time(), 0, 8) . '00') - 1;
                    break;
                case 3:
                default:
                    $today = time();
                    $today = make_timestamp(date('Y', $today), date('m', $today), date('d', $today), 0, 0, 0);
                    $timestart = $today;
                    break;
            }
            if ($product->enrolperiod <= 0) {
                $timeend = 0;
            } else {
                $timeend = $timestart + intval($product->enrolperiod);
            }

            if ($product->enrolexpirynotify == 2) {
                $expirynotify = 1;
                $notifyall = 1;
            } else {
                $notifyall = 0;
            }

            if ($instance = $DB->get_record('enrol', array('courseid' => $course->id, 'enrol' => 'ecommerce', 'customint1' => $product->id))) {
                // update existing instance

                if ($instance->enrolperiod != $product->enrolperiod or
                        $instance->expirynotify != $product->enrolexpirynotify or
                        $instance->notifyall != $notifyall or
                        $instance->expirythreshold != $product->enrolexpirythreshold) {

                    $data = new stdClass();
                    $data->enrolperiod = $product->enrolperiod;
                    $data->expirynotify = $product->enrolexpirynotify;
                    $data->notifyall = $notifyall;
                    $data->expirythreshold = $product->enrolexpirythreshold;

                    $plugin->update_instance($instance, $data);
                }

            } else {
                // create new enrollment method on course
                $data = array(
                        'name'            => $product->name,
                        'customint1'      => $product->id,
                        'status'          => $plugin->get_config('status'),
                        'roleid'          => intval($defaultrole),
                        'enrolperiod'     => $product->enrolperiod,
                        'expirynotify'    => $product->enrolexpirynotify,
                        'notifyall'       => $notifyall,
                        'expirythreshold' => $product->enrolexpirythreshold,
                );

                if ($instanceid = $plugin->add_instance($course, $data)) {
                    $instance = $DB->get_record('enrol', array('id' => $instanceid));
                }
            }

            if ($instance->id) {

                // insert log
                $enrol_exists = $DB->get_records('enrol_ecommerce', array('productid'=>$product->id, 'userid'=>$userid, 'courseid'=>$course->id, 'enrolid'=>$instance->id, 'checkoutid'=>$checkout->id));
                if (!count($enrol_exists)) {
                    $new_enrollment = new stdClass();
                    $new_enrollment->userid       = $userid;
                    $new_enrollment->productid    = $product->id;
                    $new_enrollment->courseid     = $course->id;
                    $new_enrollment->enrolid      = $instance->id;
                    $new_enrollment->checkoutid   = $checkout->id;
                    $new_enrollment->timecreated  = time();
                    $DB->insert_record('enrol_ecommerce', $new_enrollment);
                }

                $plugin->enrol_user($instance, $userid, intval($defaultrole), $timestart, $timeend);
            }
        }
    }
}

function enrol_ecommerce_delete_userdata($courseid = 0, $userid = 0) {
    global $DB;

    $dbman = $DB->get_manager();
    $DB->delete_records_select('course_modules_completion',
            'coursemoduleid IN (SELECT id FROM mdl_course_modules WHERE course=?) AND userid=?',
            array($courseid, $userid));
    $DB->delete_records('course_completions', array('course' => $courseid, 'userid' => $userid));
    $DB->delete_records('course_completion_crit_compl', array('course' => $courseid, 'userid' => $userid));
    if ($dbman->table_exists('choice_answers')) {
        $DB->delete_records_select('choice_answers',
                'choiceid IN (SELECT id FROM mdl_choice WHERE course=?) AND userid=?',
                array($courseid, $userid));
    }
    if ($dbman->table_exists('scorm_scoes_track')) {
        $DB->delete_records_select('scorm_scoes_track',
                'scormid IN (SELECT id FROM mdl_scorm WHERE course=?) AND userid=?',
                array($courseid, $userid));
    }
    if ($dbman->table_exists('quiz')) {
        $orphanedattempts = $DB->get_records_sql_menu("
                                SELECT id, uniqueid
                                  FROM {quiz_attempts}
                                WHERE userid=:userid AND quiz IN (SELECT id FROM mdl_quiz WHERE course=:courseid)", array('userid' => $userid, 'courseid' => $courseid));
        if ($orphanedattempts) {
            foreach ($orphanedattempts as $attemptid => $usageid) {
                question_engine::delete_questions_usage_by_activity($usageid);
                $DB->delete_records('quiz_attempts', array('id' => $attemptid));
            }
        }
    }

    // remove lesson info
    $lessonssql = "SELECT l.id
                                         FROM {lesson} l
                                        WHERE l.course=:course";
    $params = array ("course" => $courseid);
    $lessons = $DB->get_records_sql($lessonssql, $params);

    $params = array ("course" => $courseid, 'userid' => $userid);
    $DB->delete_records_select('lesson_timer', "lessonid IN ($lessonssql) AND userid = :userid", $params);
    $DB->delete_records_select('lesson_grades', "lessonid IN ($lessonssql) AND userid = :userid", $params);
    $DB->delete_records_select('lesson_attempts', "lessonid IN ($lessonssql) AND userid = :userid", $params);
    $DB->delete_records_select('lesson_branch', "lessonid IN ($lessonssql) AND userid = :userid", $params);
    $DB->delete_records_select('lesson_overrides', "lessonid IN ($lessonssql) AND userid = :userid", $params);
}


